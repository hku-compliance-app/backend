/* eslint-disable no-console */
/**
 * Use this script to exceed rate limit for heroku
 */
import _ from 'lodash';
import '../src/bootstrap';
import {heroku} from '../src/services/HerokuService';

const limit = 4500;

Promise.map(_.range(limit + 1), (n) =>
  heroku.get('/apps')
    .then(() => console.log(n, 'ok'))
    .catch((e) => console.log(n, 'error', e.message)),
{concurrency: 100});
