/* eslint-disable no-console, no-magic-numbers */
/**
 * Use this script to create apps in a team
 */
import _ from 'lodash';
import config from 'config';
import '../src/bootstrap';
import {heroku} from '../src/services/HerokuService';

const NUMBER_OF_APPS = 15;
const PREFIX = 'app-gdndh-';

Promise.map(_.range(NUMBER_OF_APPS), (n) =>
  heroku.post('/teams/apps', {
    body: {
      name: PREFIX + n,
      team: config.TEAM_ID,
      locked: n % 2 === 0,
      region: 'us',
      stack: 'cedar-14',
    },
  })
    .then(() => console.log(n, 'ok'))
    .catch((e) => console.log(n, 'error', e.message)),
{concurrency: 10});
