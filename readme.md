
# Heroku Compliance API
Deployment Guide

### Description

## Prerequisites
1. Node 8.9
  1. NPM 5.5
2. Postgres 10


## Configuration
Located under `config/default.json` override production settings in `config/production.json`

- **PORT** the port to listen. Set automatically in heroku.
- **DATABASE_URL** the database url for postgres. Set automatically in heroku.
- **VERBOSE_LOGGING** true if enable debug logging. Should be disabled for production.
- **MAX_RETRIES** the number of max retries when fetching resources from heroku. Max retries shouldn't be never reached in theory, but it must just prevent infinite loops if there are unexpected bugs.
- **MIN_RETRY_TIME** the minimal time when retrying heroku requests. The retry time is random between `MIN_RETRY_TIME` and `MAX_RETRY_TIME`.
- **MAX_RETRY_TIME** the maximal time when retrying heroku requests.
- **JOB_INTERVAL** the job interval when fetching data from Heroku.
- **RULES** the array of all configured rules.
- **TEAM_ID** the heroku team name or id (must be set via env variable).
- **API_TOKEN** the heroku api token (must be set via env variable). Get it from https://dashboard.heroku.com/account under "API Key".
See example values for all intervals https://www.npmjs.com/package/ms.

## Rules definition.  
Each object must contain:
- **name** the rule name. Supported only `app_name_regex`, `pipeline_name_regex` and `app_not_locked`.
- **friendlyName** the friendly name. Can be any string.
- **options** the optional rule options.

Supported rules.
- `app_name_regex` matches the application name that doesn't match given regexp.
  - `options.regexp` the regexp to use.
- `pipeline_name_regex` matches the application name that doesn't match given regexp.
  - `options.regexp` the regexp to use.
- `app_not_locked` matches the applications that are not locked.


## Sample data
1. Create a new team https://dashboard.heroku.com/teams/new
2. Use it as a `TEAM_ID` variable in the deployment section.
3. Deploy app locally or on heroku
4. Run `npm run create-apps` to create sample 15 apps. Before running this script change `PREFIX` variable in this file because app names are unique.
5. Open your team apps. https://dashboard.heroku.com/apps => "Personal" => team name.
6. Open "Apps" tab and select any app.
7. Click "More" => "Add to pipeline".
8. Click "Choose a pipeline" -> "Create new pipeline".
9. Pick any name and any stage.
10. Recommended to create two pipelines with name `<prefix>-10` and `<prefix>-7`. It will break sample configured rules.

## Local Deployment
```bash
npm i
export TEAM_ID=xyz # use `set` instead of `export` for Windows
export API_TOKEN=xyz
npm run dev # dev mode with logging
npm start # production mode
```

## Heroku Deployment
```bash
git init
git add .
git commit -m init
heroku create
heroku addons:create heroku-postgresql:hobby-dev
heroku config:set TEAM_ID=xyz API_TOKEN=xyz
git push heroku master
heroku open # get the url
```
To run npm script on heroku prefix it with `heroku run`.  
Example:
`npm run create-apps`  
to  
`heroku run npm run create-apps`


## Running Lint
```bash
npm lint
```

## Verification
Import collection and env variable from `/docs` to postman.  
Video http://take.ms/6Bshr  
  
#### Example error handling.
Change TEAM_ID to invalid value.
```
heroku config:set TEAM_ID=dkfnandmg
```
Call "Start job" and "Get job result".  
Response
```
{
    "result": "error",
    "message": "Error when fetching \"GET /teams/dkfnandmg/apps\". Message: Couldn't find that organization."
}
```

#### Rate limiting
run `npm run rate-limit` to exceed heroku rate limit and try to call "Start job".  
The application makes only two requests and it can be hard to reproduce.  


