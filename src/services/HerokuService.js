/**
 * A service for Heroku requests with rate limit handling
 */
import decorate from 'decorate-it';
import Heroku from 'heroku-client';
import config from 'config';
import ms from 'ms';
import _ from 'lodash';
import Joi from 'joi';
import logger from '../common/logger';

export const heroku = new Heroku({token: config.API_TOKEN});

// ------------------------------------
// Exports
// ------------------------------------

const HerokuService = {
  getPipelines,
  getTeamApps,
};

decorate(HerokuService, 'HerokuService');

export default HerokuService;

/**
 * Try to fetch heroku resource
 * @param {String} signature the signature used for logging
 * @param {Function} fn the heroku request factory
 * @param {Number} tryNumber the current try number
 * @returns {Object} the api response
 */
function _tryFetch(signature, fn, tryNumber = 0) {
  if (tryNumber >= config.MAX_RETRIES) {
    throw new Error(`Error when fetching "${signature}" exceeded max number of tries`);
  }
  return fn()
    .catch((e) => {
      // retry only if:
      // - no status code (no internet connection)
      // - internal errors, bad gateways etc
      // - rate limit
      const internalErrorCode = 500;
      const rateLimitErrorCode = 429;
      if (!e.statusCode || e.statusCode >= internalErrorCode || e.statusCode === rateLimitErrorCode) {
        const delay = _.random(ms(config.MIN_RETRY_TIME), ms(config.MAX_RETRY_TIME));
        logger.debug(`Retrying ${signature} with delay ${delay}ms`);
        return Promise.delay(delay)
          .then(() => _tryFetch(signature, fn, tryNumber + 1));
      }
      throw new Error(`Error when fetching "${signature}". Message: ${_.get(e, 'body.message', e.message)}`);
    });
}

getPipelines.params = [];
getPipelines.schema = {};

/**
 * Get all pipelines
 * @returns {Object} result
 */
async function getPipelines() {
  return await _tryFetch('GET /pipelines', () => heroku.get('/pipelines'));
}

getTeamApps.params = ['team'];
getTeamApps.schema = {
  team: Joi.string().required(),
};

/**
 * Get all apps that belong to any team
 * @param {String} team the team name
 * @returns {Object} result
 */
async function getTeamApps(team) {
  const url = `/teams/${team}/apps`;
  return await _tryFetch(`GET ${url}`, () => heroku.get(url));
}
