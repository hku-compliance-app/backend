/**
 * A service for retrieving rules
 */
import decorate from 'decorate-it';
import config from 'config';
// ------------------------------------
// Exports
// ------------------------------------

const RulesService = {
  getRules,
};

decorate(RulesService, 'RulesService');

export default RulesService;

getRules.params = [];
getRules.schema = {
};

/**
 * Get configured rules
 * @returns {Array} rules
 */
async function getRules() {
  return config.RULES;
}
