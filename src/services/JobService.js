/**
 * A service for retrieving rules
 */
import decorate from 'decorate-it';
import config from 'config';
import HttpErrors from 'http-errors';
import _ from 'lodash';
import ms from 'ms';
import {pick} from 'lodash/fp';
import {JobInfo} from '../models';
import logger from '../common/logger';
import {Rules} from '../Const';
import HerokuService from './HerokuService';

// ------------------------------------
// Exports
// ------------------------------------

const JobService = {
  start,
  getJobInfo,
  getJobResult,
};

decorate(JobService, 'JobService');

export default JobService;


let isRunning = false;
let timeoutId = null;


/**
 * Get items that break the given rule
 * @param {Object} rule the rule definition
 * @param {Object} data the data to check
 * @returns {Array} the matched items
 */
function _getRuleMatches(rule, {pipelines, apps}) {
  const getRegexp = () => new RegExp(rule.options.regexp);
  const serialize = pick(['name']);

  switch (rule.name) {
    case Rules.APP_NAME_REGEX: {
      const reg = getRegexp();
      return apps.filter((app) => !reg.test(app.name)).map(serialize);
    }
    case Rules.APP_NOT_LOCKED: {
      return apps.filter((app) => !app.locked).map(serialize);
    }
    case Rules.PIPELINE_NAME_REGEX: {
      const reg = getRegexp();
      return pipelines.filter((app) => !reg.test(app.name)).map(serialize);
    }
    default:
      return [];
  }
}

/**
 * Start the job
 * @returns {Object} the job result;
 */
async function _startInner() {
  const rules = config.RULES;
  // don't fetch the data if rules don't require it
  const dataToFetch = {
    pipelines: false,
    apps: false,
  };
  const grouped = _.groupBy(rules, 'name');
  if (grouped[Rules.APP_NAME_REGEX] || grouped[Rules.APP_NOT_LOCKED]) {
    dataToFetch.apps = true;
  }
  if (grouped[Rules.PIPELINE_NAME_REGEX]) {
    dataToFetch.pipelines = true;
  }
  const {pipelines, apps} = await Promise.props({
    pipelines: dataToFetch.pipelines ? HerokuService.getPipelines() : Promise.resolve(null),
    apps: dataToFetch.apps ? HerokuService.getTeamApps(config.TEAM_ID) : Promise.resolve(null),
  });
  return {
    result: 'success',
    rules: rules.map((rule) => ({
      ...rule,
      items: _getRuleMatches(rule, {pipelines, apps}),
    })),
  };
}

start.params = [];
start.schema = {};

/**
 * Start the job, and schedule the next job if previous is completed.
 */
function start() {
  if (isRunning) {
    return;
  }
  clearTimeout(timeoutId);
  const started = Date.now();
  isRunning = true;
  _startInner()
    .catch((e) => {
      logger.error(e);
      return {result: 'error', message: e.message};
    })
    .then((data) => JobInfo.create({
      started: new Date(started),
      duration: Date.now() - started,
      data,
    }))
    .finally(() => {
      isRunning = false;
      timeoutId = setTimeout(start, ms(config.JOB_INTERVAL));
    });
}

/**
 * Get the latest job
 * @returns {Object} job
 */
async function _getLatestJob() {
  return await JobInfo.findOne({
    order: [['id', 'DESC']],
  }).then((job) => {
    if (!job) {
      throw new HttpErrors.NotFound('No jobs found');
    }
    return job;
  });
}


getJobInfo.params = [];
getJobInfo.schema = {};

/**
 * Get the latest job info
 * @returns {Object} data
 */
async function getJobInfo() {
  const job = await _getLatestJob();
  return _.pick(job, 'started', 'duration');
}

getJobResult.params = [];
getJobResult.schema = {};
/**
 * Get the latest job result
 * @returns {Object} data
 */
async function getJobResult() {
  const job = await _getLatestJob();
  return job.data;
}

