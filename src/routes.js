import RulesController from './controllers/RulesController';
import JobController from './controllers/JobController';

export default {
  '/rules': {
    get: {
      method: RulesController.getRules,
      public: true,
    },
  },
  '/job/start': {
    post: {
      method: JobController.start,
      public: true,
    },
  },
  '/job/info': {
    get: {
      method: JobController.getJobInfo,
      public: true,
    },
  },
  '/job/result': {
    get: {
      method: JobController.getJobResult,
      public: true,
    },
  },
};
