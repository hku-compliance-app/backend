/**
 * A controller for retrieving rules
 */
import RulesService from '../services/RulesService';

export default {
  getRules,
};

/**
 * Get all rules
 * @param {Object} req
 * @param {Object} res
 */
async function getRules(req, res) {
  res.json(await RulesService.getRules());
}
