/**
 * A controller for retrieving rules
 */
import JobService from '../services/JobService';

export default {
  start,
  getJobInfo,
  getJobResult,
};

/**
 * Start the job
 * @param {Object} req
 * @param {Object} res
 */
async function start(req, res) {
  JobService.start();
  res.json({
    message: 'success',
  });
}

/**
 * Get the job info
 * @param {Object} req
 * @param {Object} res
 */
async function getJobInfo(req, res) {
  res.json(await JobService.getJobInfo());
}

/**
 * Get the job result
 * @param {Object} req
 * @param {Object} res
 */
async function getJobResult(req, res) {
  res.json(await JobService.getJobResult());
}
