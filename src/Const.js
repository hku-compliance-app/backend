
export const Rules = {
  APP_NAME_REGEX: 'app_name_regex',
  PIPELINE_NAME_REGEX: 'pipeline_name_regex',
  APP_NOT_LOCKED: 'app_not_locked',
};

export default {
  Rules,
};
