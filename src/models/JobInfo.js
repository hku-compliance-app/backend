export default (sequelize, DataTypes) => sequelize.define('JobInfo', {
  started: DataTypes.DATE,
  duration: DataTypes.INTEGER,
  data: DataTypes.JSON,
});
