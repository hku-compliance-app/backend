import path from 'path';
import Sequelize from 'sequelize';
import config from 'config';
import logger from '../common/logger';

export const sequelize = new Sequelize(config.DATABASE_URL, {
  dialect: 'postgres',
  operatorsAliases: Sequelize.Op,
  logging: config.VERBOSE_LOGGING ? (obj) => logger.debug(obj) : false,
});

export const JobInfo = sequelize.import(path.join(__dirname, '/JobInfo'));
