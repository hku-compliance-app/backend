/* eslint-disable no-magic-numbers */
/**
 * Configure all libraries
 */

import bluebird from 'bluebird';
import decorate from 'decorate-it';
import config from 'config';

global.Promise = bluebird;
require('babel-runtime/core-js/promise').default = bluebird; // eslint-disable-line import/no-commonjs

decorate.configure({
  debug: config.VERBOSE_LOGGING,
});

if (!config.API_TOKEN) {
  throw new Error('API_TOKEN is not set');
}

if (!config.TEAM_ID) {
  throw new Error('TEAM_ID is not set');
}
